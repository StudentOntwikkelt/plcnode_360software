<?php

include("config.php");

if (isset($_GET['pk'])) {
    $pk = trim($_GET['pk']);
    $query = trim($_GET['query']);
    $condition = trim($_GET['condition']);
    $query_type = trim($_GET['query_type']);
    $formquery = "SELECT * FROM plcnode_variabelen WHERE name = '$pk'";
    pg_query($conn, $formquery);
    while ($row = pg_fetch_assoc($formresult)) 
    {
        $value = 0;
        $type  = $row["fk_type_id"];
    }
    $insertquery = "INSERT INTO plcnode_mapper (fk_variabelen, query, condition, query_type) VALUES ('$pk', '$query', '$condition', '$query_type')";
    $insert = pg_query($conn, $insertquery);

    header('Location: '.$_SERVER['PHP_SELF'].'');
}


if (isset($_GET['edit_id'])) {
    $pk = trim($_GET['edit_id']);
    $edit_query = trim($_GET['edit_query']);
    $query = "UPDATE plcnode_mapper SET query = '$edit_query' WHERE fk_variabelen = '$pk'";
    $formresult = pg_query($conn, $query);
    
    header('Location: '.$_SERVER['PHP_SELF'].'');
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PLC-Node</title>
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <link href="../vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="../vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <link href="../vendors/starrr/dist/starrr.css" rel="stylesheet">
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../build/css/huisstijl.css">
  </head>

  <body class="nav-sm">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
          <?php
            include_once('sidebar.php');
            echo $sidebaritems;

          ?>

      
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav class="" role="navigation">
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">

            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">




                   <div class="" role="tabpanel" data-example-id="togglable-tabs">

                      <div id="myTabContent2" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content11" aria-labelledby="home-tab">
                          <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                    <table class="table">
                      <tr>
                        <th>name</th>
                        <th>voorwaarden</th>
                        <th>wacht voorwaarden</th>
                        <th>reset value</th>
                        <th>type</th>
                        <th>query</th>
                        <th>Actie</th>
                      </tr>
                          <tr>
                      <?php
                        $querynew = "SELECT * FROM plcnode_variabelen WHERE name NOT IN(SELECT fk_variabelen FROM plcnode_mapper)";
                        $newresult = pg_query($conn, $querynew);


                      ?>
                      <td>
                      <form method="get" action="plcnode.php" >
                      <select class="form-control" name="pk" style="width:50%">
                        <?php while ($row = pg_fetch_assoc($newresult)) {
                          echo "<option>" . $row['name'] . "</option>";
                        } ?>
                      </select>
                      </td>
                      <td>
                      <select class="form-control" style="width:50%" name="condition">
                      <?php
                        $querynew = "SELECT c_id, c_name FROM plcnode_conditional_mapper";
                        $newresult = pg_query($conn, $querynew);
                        while ($row = pg_fetch_assoc($newresult)) {
                          echo "<option value='" .  $row['c_id'] . "'>" . $row['c_name'] . "</option>";
                          }
                      ?>
                      </select>
                      </td>
                      <td></td>
                      <td></td>
                      <td>                      
                        <select class="form-control" name="query_type">
                          <option value="update">update</option>
                          <option value="insert">insert</option>
                        </select>
                      </td>
                      <td>
                        <TEXTAREA class="form-control" name="query">
                          
                        </TEXTAREA>
                        
                      </td>
                      <td><input type="submit" class="btn btn-success" value="Sla op">
                        </form></td>
                      </tr>                  
                      <?php
                        $getquery = "SELECT * FROM plcnode_mapper INNER JOIN plcnode_variabelen ON name = fk_variabelen LEFT JOIN plcnode_types ON fk_type_id = t_id LEFT JOIN plcnode_conditional_mapper ON c_id = condition";
                        $result = pg_query($conn, $getquery); 
                        
                        while ($row = pg_fetch_assoc($result)) 
                        { // plcnode settings

                        $rownaam = $row['fk_variabelen']; // naam 
                        $query = "SELECT * FROM plcnode_variabelen WHERE name = '$rownaam'";
                        $variabelenresult = pg_query($conn, $query);

                        ?>
                        
                        <tr>
                          <td>
                            <?php
                       
                              echo $row['fk_variabelen'];
                       
                            ?>
                            
                          </td>
                          <td><?php echo $row['c_name']; ?></td>
                          <td><?php echo $row['wachten_voorwaarden']; ?></td>
                          <td><?php echo $row['reset_value']; ?></td>
                          <td><?php echo $row['query_type']; ?></td>    
                          <td><form action="plcnode.php" method="get">
                          <input type="hidden" name="edit_id" value='<?php echo($row['fk_variabelen']); ?>'><textarea name="edit_query" class="form-control"><?php echo $row['query']; ?></textarea></td>
                          <td><input type="submit" class="btn btn-primary" name="" value="Sla op"></form></td>

                        </tr>
                      
                        <?php 

                        }

                        ?>
                      </table>
                      </form>


                        </div>


                        <!-- full query !-->
                        <div role="tabpanel" class="tab-pane fade" id="tab_content22" aria-labelledby="profile-tab">
                          



                   
                        </div>
           
                      </div>
                    </div>
                    <br />
                   
                  </div>
                  
                </div>

              </div>
            </div>


      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="js/moment/moment.min.js"></script>
    <script src="js/datepicker/daterangepicker.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="../vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="../vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Switchery -->
    <script src="../vendors/switchery/dist/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="../vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- Parsley -->
    <script src="../vendors/parsleyjs/dist/parsley.min.js"></script>
    <!-- Autosize -->
    <script src="../vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- starrr -->
    <script src="../vendors/starrr/dist/starrr.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

    <!-- bootstrap-daterangepicker -->
    <script>
      $(document).ready(function() {
        $('#birthday').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_4"
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });
      });
    </script>
    <!-- /bootstrap-daterangepicker -->

  
  </body>
</html>