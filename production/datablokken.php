<?php

include("config.php");

if (isset($_GET['datablok_name'])) {
    $naam = trim($_GET['datablok_name']);
    $formquery = "INSERT INTO plcnode_datablocks (db_name) VALUES ('$naam')";
    pg_query($conn, $formquery);
     header('Location: '.$_SERVER['PHP_SELF'].''); 
}
if (isset($_GET['datablok_level1_name'])) {
    $naam = trim($_GET['datablok_level1_name']);
    $formquery = "INSERT INTO plcnode_datablock_level1 (l1_name) VALUES ('$naam')";
    pg_query($conn, $formquery);
    header('Location: '.$_SERVER['PHP_SELF'].''); 
}
if (isset($_GET['datablok_level2_name'])) {
    $naam = trim($_GET['datablok_level2_name']);
    $formquery = "INSERT INTO plcnode_datablock_level2 (l2_name) VALUES ('$naam')";
    pg_query($conn, $formquery);
    header('Location: '.$_SERVER['PHP_SELF'].'');
}
if (isset($_GET['table'])) {
  if ($_GET['table'] == "plcnode_datablocks") {
    $value = $_GET['id'];
    $id = "db_id";
    $table = $_GET['table'];
  }
  if ($_GET['table'] == "plcnode_datablock_level1") {
    $value = $_GET['id'];
    $id = "l1_id";
    $table = $_GET['table'];
  }
  if ($_GET['table'] == "plcnode_datablock_level2") {
    $value = $_GET['id'];
    $id = "l2_id";
    $table = $_GET['table'];
  }
  $query = "DELETE FROM ".$table." WHERE ".$id." = '$value'";
  pg_query($conn, $query);
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PLC-Node</title>
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <link href="../vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="../vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <link href="../vendors/starrr/dist/starrr.css" rel="stylesheet">
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../build/css/huisstijl.css">
  </head>

  <body class="nav-sm">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
          <?php
            include_once('sidebar.php');
            echo $sidebaritems;

          ?>

      
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav class="" role="navigation">
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>PLC Node</h3>
              </div>

            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Data blokken aanmaken</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">




                   <div class="" role="tabpanel" data-example-id="togglable-tabs">


                      <ul id="myTab1" class="nav nav-tabs bar_tabs right" role="tablist">
                        <li role="presentation" class="active">
                        <a href="#tab_content11" id="home-tabb" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Blokken wijzigen</a>
                        </li>
                        
                        <li role="presentation" class="">
                        <a href="#tab_content22" role="tab" id="profile-tabb" data-toggle="tab" aria-controls="profile" aria-expanded="false">Blokken level 1 wijzigen</a>
                        </li>

                        <li role="presentation" class="">
                        <a href="#tab_content2" role="tab" id="profile-tabb" data-toggle="tab" aria-controls="profile" aria-expanded="false">Blokken level 2 wijzigen</a>
                        </li>
                      

                      
                      </ul>
                      <div id="myTabContent2" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content11" aria-labelledby="home-tab">
                          <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                    <table class="table">
                      <tr>
                        <th>Naam</th>
                        <th>Actie</th>
                      </tr>
                      
                      <tr>
                        <form method="get" action="datablokken.php">
                        <td><input type="text" name="datablok_name" class="form-control" placeholder="Naam van datablok"></td>
                        <td><input type="submit" class="btn btn-success" name="" value="Toevoegen"></td>
                        </form>
                      </tr>                    
                      <?php
                        $getquery = "SELECT * FROM plcnode_datablocks";
                        $result = pg_query($conn, $getquery); 
                        
                        while ($row = pg_fetch_assoc($result)) 
                        { // plcnode settings

                        echo "<form method='GET' action='datablokken.php'><tr>";
                        echo "<td><input type='hidden' name='id' value='".$row['db_id']."'>" . $row['db_name'] . "<input type='hidden' value='plcnode_datablocks' name='table'></td>";
                        if ($row['db_name'] == "Geen") {
                          echo "<td>Niet verwijderbaar</td>";
                        }
                        else {
                          echo "<td><input type='submit' class='btn btn-danger' value='Verwijder'></td>";
                        }
                        
                        echo "</tr></form>";
                      }
                        ?>
                      
                      </table>
                      </form>


                        </div>


                        <!-- full query !-->
                        <div role="tabpanel" class="tab-pane fade" id="tab_content22" aria-labelledby="profile-tab">
                            <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                    <table class="table">
                      <tr>
                        <th>Naam</th>
                        <th>Actie</th>
                      </tr>
                      
                      <tr>
                        <form method="get" action="datablokken.php">
                        <td><input type="text" name="datablok_level1_name" class="form-control" placeholder="Naam van datablok"></td>
                        <td><input type="submit" class="btn btn-success" name=""></td>
                        </form>
                      </tr>                    
                      <?php
                        $getquery = "SELECT * FROM plcnode_datablock_level1";
                        $result = pg_query($conn, $getquery); 
                        
                        while ($row = pg_fetch_assoc($result)) 
                        { // plcnode settings
                          echo "";
                        echo "<tr>";
                        echo "<td>" . $row['l1_name'] . "</td>";
                        echo "<td><form method='GET' action='datablokken.php'>
                        <input type='hidden' name='table' value='plcnode_datablock_level1'><input type='hidden' name='id' value='".$row['l1_id']."'><input type='submit' class='btn btn-danger' value='Verwijderen'>
                        </form></td>";
                        echo "</tr>";
                        echo "";
                      }
                        ?>
                      
                      </table>
                      </form>



                 
                        </div>

                           <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                            
                    <table class="table">
                      <tr>
                        <th>Naam</th>
                        <th>Actie</th>
                      </tr>
                      
                      <tr>
                        <form method="get" action="datablokken.php">
                        <td><input type="text" name="datablok_level2_name" class="form-control" placeholder="Naam van datablok"></td>
                        <td><input type="submit" class="btn btn-success" name=""></td>
                        </form>
                      </tr>                    
                      <?php
                        $getquery = "SELECT * FROM plcnode_datablock_level2";
                        $result = pg_query($conn, $getquery); 
                        
                        while ($row = pg_fetch_assoc($result)) 
                        { // plcnode settings

                        echo "<tr>";
                        echo "<td>" . $row['l2_name'] . "</td>";
                        echo "<td><form method='GET' action='datablokken.php'>
                        <input type='hidden' name='table' value='plcnode_datablock_level2'><input type='hidden' name='id' value='".$row['l2_id']."'><input type='submit' class='btn btn-danger' value='Verwijderen'>
                        </form></td>";
                        echo "</tr>";
                      }
                        ?>
                      
                      </table>
                      </form>



                 
                        </div>
           
                      </div>
                    </div>
                    <br />
                   
                  </div>
                  
                </div>

              </div>
            </div>


      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="js/moment/moment.min.js"></script>
    <script src="js/datepicker/daterangepicker.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="../vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="../vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Switchery -->
    <script src="../vendors/switchery/dist/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="../vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- Parsley -->
    <script src="../vendors/parsleyjs/dist/parsley.min.js"></script>
    <!-- Autosize -->
    <script src="../vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- starrr -->
    <script src="../vendors/starrr/dist/starrr.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

    <!-- bootstrap-daterangepicker -->
    <script>
      $(document).ready(function() {
        $('#birthday').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_4"
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });
      });
    </script>
    <!-- /bootstrap-daterangepicker -->

  
  </body>
</html>