/*global require,console,setTimeout */
var opcua = require("node-opcua");
var async = require("async");
var app = require('express')();
var fs = require('fs');

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index2.html');
});

var settings = JSON.parse(
    fs.readFileSync("settings.json")
); 

// BEGIN CONFIG //
var client = new opcua.OPCUAClient({
   keepSessionAlive:  true,
       connectionStrategy: {
        maxRetry: 100000
    }
  //
});
var endpointUrl = settings.opcua;

var config = {
  user: settings.pgUser, 
  database: settings.pgDatabase,
  password: settings.pgPassword,
  host: settings.pgHost,
  port: settings.pgPort,
};
// EINDE CONFIG //

var the_session, the_subscription, newsub;
var userIdentity  = null;
var pg = require('pg');
//var mysql     =    require('mysql');
const sprintfJs = require('sprintf-js')


//this initializes a connection pool
//it will keep idle connections open for a 30 seconds
//and set a limit of maximum 10 idle clients
var pool = new pg.Pool(config);

// to run a query we can acquire a client from the pool,
// run a query on the client, and then return the client to the pool
pool.connect(function(err, client, done) 
{
  if(err) {
    return console.error('error fetching client from pool', err);
  }
  client.query('SELECT *, count(*) OVER(partition by c_name) as count_wacht_vars FROM plcnode_mapper '                                
                                + 'INNER JOIN plcnode_variabelen ON plcnode_variabelen.name = plcnode_mapper.fk_variabelen '
                                + 'LEFT JOIN plcnode_groups ON plcnode_variabelen.fk_group_id = plcnode_groups.g_id '
                                + 'INNER JOIN plcnode_datablocks ON plcnode_datablocks.db_id = plcnode_variabelen.fk_datablock_id '
                                + 'INNER JOIN plcnode_datablock_level1 ON plcnode_datablock_level1.l1_id = plcnode_variabelen.fk_datablock_level1 '
                                + 'LEFT JOIN plcnode_conditional_mapper ON c_id = condition;', function(err, result) {
    //call `done()` to release the client back to the pool
    done();

    if(err) {
      return console.error('error running query', err);
    }
    global.getal = result.rows.length;


    global.result = result;

    //output: 1
  });
});

function getWachtVarRow (fk_variabelen_naam, client) 
{
    return new Promise(function (resolve, reject) {
      var queryGetWachtData = "SELECT * FROM  plcnode_mapper WHERE fk_variabelen = '" + fk_variabelen_naam + "';";
      console.log("Query get wacht data" + queryGetWachtData);
      client.query(queryGetWachtData, function(err, resultaat) {
          if(err) {
            return reject(err);
          }
          resolve(resultaat);
      });   
    });
}

function getVarName (variabele_naam , client) 
{
    // return new Promise(function (resolve, reject) 
    // {
    //   var queryGetWachtData = "SELECT CASE WHEN fk_datablock_id > 0 AND fk_datablock_level1 > 0 AND fk_datablock_level2 = 0 THEN CONCAT('"',db_name,'"','."',l1_name,'"','."',name,'"')  WHEN fk_datablock_id = 0 AND fk_datablock_level1 = 0 AND fk_datablock_level2 = 0 THEN name END AS variabele_naam FROM plcnode_variabelen RIGHT JOIN plcnode_datablocks ON fk_datablock_id = db_id RIGHT JOIN plcnode_datablock_level1 ON plcnode_datablock_level1.l1_id = plcnode_variabelen.fk_datablock_level1 RIGHT JOIN plcnode_datablock_level2 ON plcnode_datablock_level2.l2_id = plcnode_variabelen.fk_datablock_level2 WHERE name = '" + variabele_naam + "';";
    //   console.log("Query get wacht data" + queryGetWachtData);
    //   client.query(queryGetWachtData, function(err, resultaat) {
    //       if(err) {
    //         return reject(err);
    //       }
    //       resolve(resultaat);
    //   });   
    // });
}

function zoekcId (fknaam, client) 
{
  return new Promise(function(resolve, reject) {
    var query = "SELECT c_id FROM plcnode_conditional_mapper WHERE c_name = '" + fknaam + "';";
    client.query(query, function(err, cIdResultaat) {
      if(err) {
        return reject(err);
      }
      resolve(cIdResultaat);
    }); 
  });
}



function variabeleInsertsLezen (c_id, client) 
{
  return new Promise(function (resolve, reject) 
  {
    var queryGetWachtData = "SELECT * FROM  plcnode_mapper WHERE condition = '" + c_id + "';";

    client.query(queryGetWachtData, function(err, resultaat) {
        if(err) {
          return reject(err);
        }
        resolve(resultaat);
    });   
  });
}

function extraVariabeleLezen (variabele_naam) 
{
  return new Promise(function (resolve, reject) 
  {
          // const formattedQuery = sprintfJs.sprintf(orgquery, object2)
          // console.log(formattedQuery);

      console.log(variabele_naam);

      the_session.readVariableValue("ns=3;s=" + variabele_naam, function(err,dataValue) 
      {
        // Laatste wijziging: 20-6-2017

        if (dataValue.value.value != null)
        {
          resolve(dataValue.value.value);
        } else {
          resolve(0);
        }
      })

  });
}

var query2 = [];

function variabeleUitPlcLezenEnReplacen (variabeleNaam, afgekorte_variabele, query, client, lengte, totalelengtevanresult, variabel, reset_value) 
{
    return new Promise(function (resolve, reject) {
      the_session.readVariableValue("ns=3;s=" + variabeleNaam, function(err,dataValue) 
      {
        query2[afgekorte_variabele] = dataValue.value.value; 
        totalelengtevanresult = totalelengtevanresult + 1;

        if(totalelengtevanresult == lengte) {
          console.log(insertQueryReplacen(query, dataValue.value.value, query2, client, lengte, variabel, reset_value));
        }
      })

    });
}

function monitorPLCvariabele(newSub, id, voorwaarden, wacht_variabele, wachten_voorwaarden, variabel, query, query_type, condition, extravalue1)
{
    // var newSub = new opcua.ClientSubscription(the_session,{
    //         requestedPublishingInterval: 100,
    //         requestedLifetimeCount: 10,
    //         requestedMaxKeepAliveCount: 2,
    //         maxNotificationsPerPublish: 10,
    //         publishingEnabled: true,
    //         priority: 10
    //     });
    var monitoredItem  = newSub.monitor({
          nodeId: opcua.resolveNodeId(id),
          attributeId: 13
        //, dataEncoding: { namespaceIndex: 0, name:null }
      },
      {
          samplingInterval: 100,
          discardOldest: true,
          queueSize: 10
      });
      
      console.log("-------------------------------------");

      // BEGIN ON CHANGE PLC VALUE //
      monitoredItem.on("changed",function(dataValue)
      {
          
          // BEGIN POSTGRESS CONNECTION //
          pool.connect(function(err, client, done) 
          {

            if(err) {
              return console.error('error fetching client from pool', err);
            }

            if (wachten_voorwaarden == null)
            {
              wachten_voorwaarden = true;
            }

            console.log(voorwaarden);
            
            // BEGIN IS HET EEN WACHT VARIABELEN : JA / NEE //
            if(wacht_variabele == true) 
            {
                // BEGIN ALS DE WACHTVOORWAARDEN GELIJK IS AAN DE VOORWAARDEN WAAROP GEWACHT MOET WORDEN, GA DOOR //
                if(dataValue.value.value == JSON.parse(wachten_voorwaarden)) 
                {
                  // BEGIN INSERT QUERY ALS WACHT VARIABELE VOLDOET AAN DE VOORWAARDE //
                  if(query_type == "insert") 
                  {
                    console.log("------------------------------------------------------------------------------------------------------------- INSERT QUERY MET WACHTVARIABELE");



                    for (i = 0; i < getal; i++) 
                    {           
                      if(result.rows[i].name == variabel)
                      { 
                          console.log("loop "+variabel);
                          reset_value = result.rows[i].reset_value; 

                          if(result.rows[i].reset_value == null) 
                          {
                              console.log("---------------------------------------------------- EEN INSERT QUERY HEEFT ALTIJD EEN RESET VALUE NODIG");
                          }
                          else
                          {
                            var queryVanInsert = result.rows[i].query;

                            zoekcId(result.rows[i].fk_variabelen, client).then(function(cIdResultaat) 
                            {                            
                              cIdResultaat =JSON.stringify(cIdResultaat.rows);
                              cIdResultaat =JSON.parse(cIdResultaat);

                              variabeleInsertsLezen(cIdResultaat[0].c_id, client).then(function(resultaat) 
                              {
                                resultaat =JSON.stringify(resultaat.rows);
                                resultaat =JSON.parse(resultaat);

                                

                                for(y = 0; y < resultaat.length; y++) 
                                {
                                  if(resultaat[y].fk_variabelen.search("\"") != -1) 
                                  {
                                    console.log("Wacht variabele: "+variabel);    
                                    console.log(resultaat[y].fk_variabelen);
                                    var waardes = resultaat[y].fk_variabelen.split("\"");
                                    var losseVariabel = waardes[waardes.length - 2];                                                              

                                    variabeleUitPlcLezenEnReplacen(resultaat[y].fk_variabelen, losseVariabel, queryVanInsert, client, resultaat.length, y, variabel, reset_value);
                                  }                      
                                }
                              }).catch((err) => setImmediate(() => { throw err; }));
                            }).catch((err) => setImmediate(() => { throw err; }));
                          }
                      } 
                    }
                  } 
                  // EINDE INSERT QUERY ALS WACHT VARIABELE VOLDOET AAN DE VOORWAARDE//
                  
                  // BEGIN UPDATE QUERY ALS WACHT VARIABELE VOLDOET AAN DE VOORWAARDE//
                  if(query_type == "update") 
                  { 
                    console.log("------------------------------------------------------------------------------------------------------------- BEGIN UPDATE QUERY MET WACHTVARIABELE");

                    for (i = 0; i < getal; i++) 
                    {           
                      if(result.rows[i].name == variabel)
                      {               
                        if(result.rows[i].reset_value != null) 
                        {
                          var reset_val = JSON.parse(result.rows[i].reset_value);
                          console.log("Reset value: " + reset_val);
                          write_node_value(variabel, reset_val, typeof(reset_val));
                          variabeleLezen(query, variabele, reset_val, false, wachten_voorwaarden, result.rows[i].c_name, client, done, reset_val, extravalue1);
                        }                 

                      }   

                      if(result.rows[i].c_name == variabel) 
                      {                
                        
                        if (result.rows[i].fk_datablock_id == 0)
                        {
                          variabele = result.rows[i].fk_variabelen;
                        }
                        else
                        {
                          if (result.rows[i].fk_datablock_level1 != 0 && result.rows[i].fk_datablock_level2 != 0)
                          {
                            variabele = '"'+result.rows[i].db_name+'"."'+result.rows[i].l1_name+'"."'+result.rows[i].l2_name+'"."'+result.rows[i].fk_variabelen+'"';
                          }
                          else if (result.rows[i].fk_datablock_level1 != 0)
                          {
                            variabele = '"'+result.rows[i].db_name+'"."'+result.rows[i].l1_name+'"."'+result.rows[i].fk_variabelen+'"';
                          }
                        }
                        
                        wacht_variabele_name = result.rows[i].c_name;
                        var q = result.rows[i].query;
                        if (q != null && q.length > 5)
                        {
                          variabeleLezen(q, variabele, dataValue.value.value, wacht_variabele, wachten_voorwaarden, result.rows[i].c_name, client, done, reset_val, extravalue1);
                        }

                      }
                    }
                   
                  }             
                  // EINDE UPDATE QUERY ALS WACHT VARIABELE VOLDOET AAN DE VOORWAARDE//

                }
                // BEGIN ALS DE WACHTVOORWAARDEN GELIJK IS AAN DE VOORWAARDEN WAAROP GEWACHT MOET WORDEN, GA DOOR //  
            } 
            else 
            {
              console.log("------------------------------------------------------------------------------------------------------------- UPDATE QUERY ZONDER WACHTVARIABELE");



              if(query_type == "update" && voorwaarden == 'Geen wachtvariabele')
              {
                console.log("------------------------------------------------------------------------------------------------------------- Klaar"+ extravalue1);


                  variabeleLezen(query, variabel, dataValue.value.value, wacht_variabele, wachten_voorwaarden, null, client, done, null, extravalue1);
               
              }                 

            }
            // EINDE IS HET EEN WACHT VARIABELEN : JA / NEE //

          });
          // EINDE POSTGRESS CONNECTION //

      });
      // EINDE ON CHANGE PLC VALUE //
}

var query3 = [];

// Bij wacht_variabele = true, worden alle variabelen uitgelezen die bij deze wacht_var horen
function variabeleLezen(q, variabele, value, wacht_value, wachten_voorwaarden, wacht_variabele_name, client, done, reset_val, query_tabel) 
{
  // console.log(variabele);

  // query3['DB_Onderdeel.Status_gestart.Truckmixer_aanwezig'] = 2; 

  //         const formattedQuery = sprintfJs.sprintf(q, query3)
  //         console.log(formattedQuery);

  if (query_tabel != null)
  {
    console.log(query_tabel);

    extraVariabeleLezen(query_tabel).then(function(extravalue1) 
    {
        var copyQuery = JSON.parse(JSON.stringify(q));

        

        copyQuery = copyQuery.replace("$value", value);
        copyQuery = copyQuery.replace("$extravalue1", extravalue1);
        console.log(copyQuery);


console.log("------------------------------------------------------------------------------------------------------------- Klaar"+ extravalue1);

        client.query(copyQuery, function(err, result) 
        {
          done();
          if(err) {
            return console.error('error running query', err);
          }

          console.log("------------------------------------------------------------------------------------------------------------- UPDATE QUERY UITGEVOERD");
        })
      
          
    }).catch((err) => setImmediate(() => { throw err; }));
  } 
  else 
  {

    if (wacht_value == true)
    {
      the_session.readVariableValue("ns=3;s=" + variabele, function(err,dataValue) 
      {
          var copyQuery = JSON.parse(JSON.stringify(q));

          copyQuery = copyQuery.replace("$value", dataValue.value.value);
          console.log(copyQuery);
          client.query(copyQuery, function(err, result) 
          {
            done();
            if(err) {
              return console.error('error running query', err);
            }

            console.log("------------------------------------------------------------------------------------------------------------- UPDATE QUERY UITGEVOERD");
          })
      })
    }
    else 
    {
      console.log("Wegschrijven value voor " + variabele);
      var copyQuery = JSON.parse(JSON.stringify(q));
      // var copyQuery = q;

      copyQuery = copyQuery.replace("$value", value);
      console.log(copyQuery);
      client.query(copyQuery, function(err, result) {
        //call `done()` to release the client back to the pool
        done();
        if(err) {
          return console.error('error running query', err);
        }
      })
    }

  }




  // if (wacht_value == true)
  // {
  //   the_session.readVariableValue("ns=3;s=" + variabele, function(err,dataValue) 
  //   {
  //       var copyQuery = JSON.parse(JSON.stringify(q));

  //       copyQuery = copyQuery.replace("$value", dataValue.value.value);
  //       console.log(copyQuery);
  //       client.query(copyQuery, function(err, result) 
  //       {
  //         done();
  //         if(err) {
  //           return console.error('error running query', err);
  //         }

  //         console.log("------------------------------------------------------------------------------------------------------------- UPDATE QUERY UITGEVOERD");
  //       })
  //   })
  // }
  // else 
  // {
  //   var copyQuery = JSON.parse(JSON.stringify(q));
  //   // var copyQuery = q;
  //   copyQuery = copyQuery.replace("$value", value);
  //   console.log(copyQuery);
  //   client.query(copyQuery, function(err, result) {
  //     //call `done()` to release the client back to the pool
  //     done();
  //     if(err) {
  //       return console.error('error running query', err);
  //     }
  //   })
  // }
}

function insertQueryReplacen(orgquery, waarde, object2, client, lengte, variabel, reset_value) 
{
    console.log("-------------------------------------------------------- INSIDE QUERY REPLACE");

    console.dir(object2);

    var objectlengte = Object.keys(object2).length;

    // Map het object op de query (automtisch)
    // INSERT INTO test (value, value2) VALUES (%(insert_test1)s, %(insert_test2)s)
    // Vervang %(insert_test1)s met object["insert_test1"]
    if(objectlengte == lengte) {
      const formattedQuery = sprintfJs.sprintf(orgquery, object2)
      console.log(formattedQuery);
      client.query(formattedQuery, function(err, result) {
          if(err) {
            return console.error('error running query', err);
          }

          console.log("Reset value: " + reset_value);
          write_node_value(variabel, reset_value, typeof(reset_value));

          console.log("------------------------------------------------------------------------------------------------------------- INSERT QUERY UITGEVOERD");
      })

    }
}

// step 5: to write value to a node
function write_node_value(variabele, value, dType) 
{        
    if (dType == "boolean")
    {
        var nodesToWrite = [
        {
              nodeId: "ns=3;s=" + variabele, //variabele
              attributeId: opcua.AttributeIds.Value,
              indexRange: null,
              value: { /* dataValue*/
                  value: { /* Variant */
                      dataType: opcua.DataType.Boolean, //DataType
                      value: value //value
                  }
            }
          }
        ];
    }
    else 
    {
        var nodesToWrite = [
        {
              nodeId: "ns=3;s=" + variabele, //variabele
              attributeId: opcua.AttributeIds.Value,
              indexRange: null,
              value: { /* dataValue*/
                  value: { /* Variant */
                      dataType: opcua.DataType.Int16, //DataType
                      value: value //value
                  }
            }
          }
        ];
    }

    console.log("write");

    the_session.write(nodesToWrite, function (err, statusCodes) {
          if (!err) {
              console.log("statusCode = ",statusCodes[0].toString());
              //statusCodes.length.should.equal(nodesToWrite.length);
              //statusCodes[0].should.eql(opcua.StatusCodes.BadNotWritable);
          }else {
              console.log("err =",err);
          }
    });
}



 var options = {
    connectionStrategy: {
        maxRetry: 3,
        initialDelay: 1000,
        maxDelay: 10000
    },
    SessionTimeout: 999999,
    keepSessionAlive: false,
               requestedLifetimeCount: 99999,
           requestedMaxKeepAliveCount: 99999,
  };
  var client = new opcua.OPCUAClient(options);


client.on('start_reconnection', function() {
  console.log("START RECONNECTION");
});
client.on('after_reconnection', function() {
  console.log("AFTER RECONNECTION");
});
client.on('backoff', function(nb, delay) 
{
    console.log("  connection failed for the", nb, " time ... We will retry in ", delay, " ms");
    if (nb == 1)
    {
          var the_session = null;
    }
});
client.on("close", function () {
  console.log("close and abort");
    opstart = true;
    skip = true;

  setTimeout(function(){
    connect();
  },1000);
});

connect();

function connect()
{

async.series([

    // step 1 : connect to
    function(callback)  {
      console.log(" Version ", opcua.version);
        client.connect(endpointUrl,function (err) {
            if(err) {
                console.log(" cannot connect to endpoint :" , endpointUrl );
            } else {
                console.log("connected !");
            }
            callback(err);
        });
        client.on('connect', function() 
        {
          console.log("START CONNECTION");
        });
    },

        // reconnect using the correct end point URL now
    function (callback) 
    {
          client.createSession( function(err,session) {
            if(!err) {
                the_session = session;
            }
            callback(err);
        });
    },


    // step 2 : createSession
    function(callback) {
        client.createSession( function(err,session) {
            if(!err) {
                the_session = session;
            }
            callback(err);
        });
    },

    // step 3 : browse
    function(callback) {
       the_session.browse("RootFolder", function(err,browse_result){
           if(!err) {
               browse_result[0].references.forEach(function(reference) {
                   console.log( reference.browseName.toString() + "Hello");
               });
           }
           callback(err);
       });
    },

    // step 4' : read a variable with read
    // function(callback) {
    //    var max_age = 0;
    //    var nodes_to_read = [
    //       { nodeId: "ns=3;s=\"Data_block_1\".\"var1\"", attributeId: opcua.AttributeIds.Value }
    //    ];
    //    console.log(nodes_to_read);
    //    the_session.read(nodes_to_read, max_age, function(err,nodes_to_read,dataValues) {
    //        if (!err) {
    //            console.log(" free mem % = " , dataValues[0]);
    //            io.emit('chat message', dataValues[0]);
    //        }
    //        callback(err);
    //    });
    // },
    
    // step 5: install a subscription and install a monitored item for 10 seconds
    function _subscribe(callback) 
    {

       the_subscription_plc =new opcua.ClientSubscription(the_session,{
                requestedPublishingInterval: 500,
                requestedLifetimeCount: 99999999,
                requestedMaxKeepAliveCount: 99999999,
                maxNotificationsPerPublish: 99999999,
                publishingEnabled: true,
                priority: 1
       });

      // the_subscription_plc =  new opcua.ClientSubscription(options);

        for (i = 0; i < getal; i++)
        {

            if (result.rows[i].fk_datablock_id == 0)
            {
              variabele = result.rows[i].fk_variabelen;
              wacht_variabel = result.rows[i].wacht_variabele;
              wachten_voorwaarden = result.rows[i].wachten_voorwaarden;
              query_type = result.rows[i].query_type;
              condition = result.rows[i].condition;
              voorwaarden = result.rows[i].c_name;
              console.log("DATA BLOCK = 0" + variabele);

              monitorPLCvariabele(the_subscription_plc, 'ns=3;s='+variabele+'', voorwaarden, wacht_variabel, wachten_voorwaarden, variabele, result.rows[i].query, query_type, condition, result.rows[i].extravalue1);

             
            }
            else
            {
               if (result.rows[i].fk_datablock_level1 != 0 && result.rows[i].fk_datablock_level2 != 0)
              {  
                wacht_variabel = result.rows[i].wacht_variabele;
                variabele = '"'+result.rows[i].db_name+'"."'+result.rows[i].l1_name+'"."'+result.rows[i].l2_name+'"."'+result.rows[i].fk_variabelen+'"';
                wachten_voorwaarden = result.rows[i].wachten_voorwaarden;
                query_type = result.rows[i].query_type;
                condition = result.rows[i].condition;
                voorwaarden = result.rows[i].c_name;
                console.log(variabele);

                monitorPLCvariabele(the_subscription_plc, 'ns=3;s='+variabele+'', voorwaarden, wacht_variabel, wachten_voorwaarden, variabele, result.rows[i].query, query_type, condition, result.rows[i].extravalue1);
              }
              else if (result.rows[i].fk_datablock_level1 != 0)
              {  
                wacht_variabel = result.rows[i].wacht_variabele;
                variabele = '"'+result.rows[i].db_name+'"."'+result.rows[i].l1_name+'"."'+result.rows[i].fk_variabelen+'"';
                wachten_voorwaarden = result.rows[i].wachten_voorwaarden;
                query_type = result.rows[i].query_type;
                condition = result.rows[i].condition;
                voorwaarden = result.rows[i].c_name;
                console.log(variabele);

                monitorPLCvariabele(the_subscription_plc, 'ns=3;s='+variabele+'', voorwaarden, wacht_variabel, wachten_voorwaarden, variabele, result.rows[i].query, query_type, condition, result.rows[i].extravalue1);
              }
            }

        }
    },

    // close session
    function(callback) {
        // the_session.close(function(err){
        //     if(err) {
        //         console.log("session closed failed ?");
        //     }
        //     callback();
        // });
    }

],
function(err) {
    if (err) {
        console.log(" failure ",err);
    } else {
        console.log("done!");
    }
    // client.disconnect(function(){});
}) ;

  
}
