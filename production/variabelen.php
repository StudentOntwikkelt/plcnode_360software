<?php

  include("config.php");

  if (isset($_GET['naam'])) {
    $naam = trim($_GET['naam']);

    $datablok = trim($_GET['datablok']);
    $datablokl1 = trim($_GET['datablokl1']);
    $datablokl2 = trim($_GET['datablokl2']);
    $type = trim($_GET['type']);
    $wacht = trim($_GET['wacht']);
    $group = trim($_GET['group']);
    $formquery = "INSERT INTO plcnode_variabelen VALUES ('$naam', $type, $datablok, $datablokl1, $datablokl2, $wacht, $group)";
    pg_query($conn, $formquery); 
    

    function stringMaker($datablok, $datablokl1, $datablokl2, $naam, $conn) {
      $query_datablok = "SELECT * FROM plcnode_datablocks WHERE db_id = '$datablok'";
      $result_datablok = pg_query($conn, $query_datablok);
      $query_datablok_l1 = "SELECT * FROM plcnode_datablock_level1 WHERE l1_id = '$datablokl1'";
      $result_datablok_l1 = pg_query($conn, $query_datablok_l1);
      $query_datablok_l2 = "SELECT * FROM plcnode_datablock_level2 WHERE l2_id = '$datablokl2'";
      $result_datablok_l2 = pg_query($conn, $query_datablok_l2);
      while ($row = pg_fetch_assoc($result_datablok)) {
          $datablok_name = $row['db_name'];
      }
      while ($row = pg_fetch_assoc($result_datablok_l1)) {
          $datablok_l1_name = $row['l1_name'];
      }
      while ($row = pg_fetch_assoc($result_datablok_l2)) {
          $datablok_l2_name = $row['l2_name'];
      }
      if ($datablok_l1_name == "Geen") {
        $datablok_l1_name_string = "";
        $datablok_l2_name_string = "";
      }
      else {
        $datablok_l1_name_string = '"'.$datablok_l1_name.'".';
        if ($datablok_l2_name == "Geen") {
          $datablok_l2_name_string = "";
        }
        else {
          $datablok_l2_name_string = '"'.$datablok_l2_name.'".';
        }
      }
     
      

      $string = '"'.$datablok_name.'".'.$datablok_l1_name_string.$datablok_l2_name_string.'"'.$naam.'"';
      $query = "INSERT INTO plcnode_conditional_mapper (c_name) VALUES ('$string')";
      pg_query($conn, $query);
    }

    if ($wacht) {
      stringMaker($datablok, $datablokl1, $datablokl2, $naam, $conn);
    }
    

    #header('Location: '.$_SERVER['PHP_SELF'].'');
  }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PLC-Node</title>
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <link href="../vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="../vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <link href="../vendors/starrr/dist/starrr.css" rel="stylesheet">
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../build/css/huisstijl.css">
  </head>

  <body class="nav-sm">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
          <?php
            include_once('sidebar.php');
            echo $sidebaritems;

          ?>

      
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav class="" role="navigation">
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>PLC Node</h3>
              </div>

            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Variabelen toevoegen</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <div class="col-sm-12">
                  
                    <div class="col-sm-6">
                     <form method="GET" action="Variabelen.php">
                      <p>Datablok*</p>
                      <select name="datablok" class="form-control">
                        <?php
                          $query = "SELECT * FROM plcnode_datablocks";
                          $result = pg_query($conn, $query);
                          while ($row = pg_fetch_assoc($result)) {
                            echo '<option value="'.$row['db_id'].'">';
                            echo $row['db_name'];
                            echo "</option>";
                          }
                        ?>
                      </select><br>
                      <p>Datablok level 1</p>
                      <select name="datablokl1" class="form-control">

                        <?php
                          $query = "SELECT * FROM plcnode_datablock_level1";
                          $result = pg_query($conn, $query);
                          while ($row = pg_fetch_assoc($result)) {
                            echo '<option value="'.$row['l1_id'].'">';
                            echo $row['l1_name'];
                            echo "</option>";
                          }
                        ?>
                      </select><br>
                      <p>Datablok level 2</p>
                      <select name="datablokl2" class="form-control">
                        <?php
                          $query = "SELECT * FROM plcnode_datablock_level2";
                          $result = pg_query($conn, $query);
                          while ($row = pg_fetch_assoc($result)) {
                            echo '<option value="'.$row['l2_id'].'">';
                            echo $row['l2_name'];
                            echo "</option>";
                          }
                        ?>
                      </select><br>
                      <p>Type</p>
                      <select name="type" class="form-control">
                        <?php
                          $query = "SELECT * FROM plcnode_types";
                          $result = pg_query($conn, $query);
                          while ($row = pg_fetch_assoc($result)) {
                            echo '<option value="'.$row['t_id'].'">';
                            echo $row['t_name'];
                            echo "</option>";
                          }
                        ?>
                      </select><br>
                      <p>Groep</p>
                      <select name="group" class="form-control">
                        <?php
                          $query = "SELECT * FROM plcnode_groups";
                          $result = pg_query($conn, $query);
                          while ($row = pg_fetch_assoc($result)) {
                            echo '<option value="'.$row['g_id'].'">';
                            echo $row['g_name'];
                            echo "</option>";
                          }
                        ?>
                      </select><br>
                      <p>Wacht variabele</p>
                     
                      <input type='hidden' value='0' name='wacht'>
                      <!-- <input type='checkbox' value='1' style="box-shadow: none !important;" class="form-control" name='wacht'><br> -->

<input type="radio" name="wacht" value="true" > Ja<br>
  <input type="radio" name="wacht" value="false" checked> nee
  <br /><br />

                      <p>Naam*</p>
                      <textarea class="form-control" name="naam" placeholder="Naam"></textarea><br>
                      <input type="submit" class="btn btn-success" value="Toevoegen" name="">
                      </form>
                    </div>
                  </div>
                    </div>
                    <br />
                   
                  </div>
                  
                </div>

              </div>
            </div>


      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="js/moment/moment.min.js"></script>
    <script src="js/datepicker/daterangepicker.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="../vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="../vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Switchery -->
    <script src="../vendors/switchery/dist/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="../vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- Parsley -->
    <script src="../vendors/parsleyjs/dist/parsley.min.js"></script>
    <!-- Autosize -->
    <script src="../vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- starrr -->
    <script src="../vendors/starrr/dist/starrr.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

    <!-- bootstrap-daterangepicker -->
    <script>
      $(document).ready(function() {
        $('#birthday').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_4"
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });
      });
    </script>
    <!-- /bootstrap-daterangepicker -->

  
  </body>
</html>